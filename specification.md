# Mini Kube

Write a simple hello world application in either of these languages: Python, Ruby, Go.

Build the application within a Docker container and then load balance the application within a mini kube.
