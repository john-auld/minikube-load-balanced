#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
  This is a simple Hello World application for demonstration purposes.
'''

# standard libraries
import json
import socket

# Third party libraries
from flask import (Flask, Response)


app = Flask(__name__)

@app.route("/", methods=['GET'])
def hello():
    '''
     Endpoint for hello world response
    '''

    hello_world_message = '<p>Hello World! from {}</p>'.format( socket.gethostname() )
    
    response = Response(
        hello_world_message,
        status=200,
        mimetype='text/html'
    )
    return response


@app.route("/healthz", methods=['GET'])
def healthz():
    '''
     Endpoint for hello world health check response
    '''
    response = Response(
        'ok',
        status=200,
        mimetype='text/plain'
    )
    return response


### Start up settings
if __name__ == "__main__":
    app.run(host="0.0.0.0")