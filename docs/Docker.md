# Build notes for the docker image

Repository: [minikube-load-balanced](https://gitlab.com/john-auld/minikube-load-balanced.git)

## Docker image

These steps have been tested on a CentOS 7 having a working docker installation.

- checkout the master branch of the repository.
- run the commands below

### Build image

```bash
docker login registry.gitlab.com

# The tag, 2.0, is an example. Use an appropriate value.
docker build -t registry.gitlab.com/john-auld/minikube-load-balanced:2.0 .

sudo docker images
  registry.gitlab.com/john-auld/minikube-load-balanced  2.0                 c13a8b2c7343        About a minute ago   932 MB
```

### Push image to the docker registry

The registry login step is included above in the build section.

```bash
docker push registry.gitlab.com/john-auld/minikube-load-balanced:1.0

In the Gitlab web UI, check that the image has been added to the registry.

https://gitlab.com/john-auld/minikube-load-balanced/container_registry
```

### Remvove test container and image on build node

These steps are optional, but they can be followed to remove temporary artifacts from the build node.

```bash
sudo docker ps -a | awk '$0 ~ /minikube-load-balanced/ {print $1}' | xargs -I % sudo docker rm -f %

sudo docker images | awk '$0 ~ /minikube-load-balanced/ {print $3}' | xargs -I % sudo docker image rm %
```
