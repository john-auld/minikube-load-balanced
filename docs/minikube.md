# Instructions for running the stock service on Kubernetes

The procedure requires a vanilla kubernetes environment. The steps here were tested on a Mac OSX release of minkkube.

## Check the status of minikube

If needed, start minikube.

```bash
minikube start
```

Check the status of minikube

```bash
minikube status
host: Running
kubelet: Running
apiserver: Running
kubectl: Correctly Configured: pointing to minikube-vm at 192.168.99.100
```

Apply the manifests in the k8s directory

```bash
kubectl apply -f k8s
```

Check the resources

```bash
kubectl get all
  NAME                               READY   STATUS    RESTARTS   AGE
  pod/hello-world-599dcc8ffb-ckzrf   1/1     Running   0          4m12s
  pod/hello-world-599dcc8ffb-hhkf8   1/1     Running   0          4m12s

  NAME                  TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
  service/hello-world   ClusterIP   10.96.108.110   <none>        80/TCP    4m12s
  service/kubernetes    ClusterIP   10.96.0.1       <none>        443/TCP   5d23h

  NAME                          READY   UP-TO-DATE   AVAILABLE   AGE
  deployment.apps/hello-world   2/2     2            2           4m12s

  NAME                                     DESIRED   CURRENT   READY   AGE
  replicaset.apps/hello-world-599dcc8ffb   2         2         2       4m12s
```

Check the Ingress controller

```bash
kubectl get pods -n kube-system | grep nginx-ingress-controller
  nginx-ingress-controller-5d9cf9c69f-45f82   1/1     Running   0          4m20m


kubectl get Ingress hello-world-ingress
  NAME                  HOSTS   ADDRESS     PORTS   AGE
  hello-world-ingress   *       10.0.2.15   80      7m58s
```

Check the logs on the pods

```bash
kubectl get pods
  NAME                           READY   STATUS    RESTARTS   AGE
  hello-world-599dcc8ffb-ckzrf   1/1     Running   0          10m
  hello-world-599dcc8ffb-hhkf8   1/1     Running   0          10m

kubectl logs hello-world-599dcc8ffb-ckzrf
  [2019-09-03 13:35:08 +0000] [1] [INFO] Starting gunicorn 19.9.0
  [2019-09-03 13:35:08 +0000] [1] [INFO] Listening at: http://0.0.0.0:8000 (1)
  [2019-09-03 13:35:08 +0000] [1] [INFO] Using worker: sync
  [2019-09-03 13:35:08 +0000] [8] [INFO] Booting worker with pid: 8
```

Check access to the hello-world application

```bash
curl $(minikube ip) && echo
  <p>Hello World!</p>
```
