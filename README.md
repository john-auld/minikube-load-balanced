# Minikube load balanced demonstration with python flask

This repository demonstrates a simple configuration to host a load balanced python flask web application on minikube.

## Requirements

- A vanilla installation of miniube
- A working docker instance
- Python 3, for development only

## Setup notes

### Clone repository and setup a development environment

```bash
git clone https://gitlab.com/john-auld/minikube-load-balanced.git
```

For development work involving changes to the python code, setup a virtual environemnt.

```bash
# Before starting work
python3 -m venv venv
source venv/bin/activate
pip install -r app/requirements.txt

# After finishing work
deactivate
```

### Configure minikube

The following steps are needed only when minikube is not running or when the ingress addon is not enabled.

```bash
minikube start
# Enable the ingress addon
minikube addons enable ingress

# Check that the ingress addon is enabled
minikube addons list | grep ingress
  - ingress: enabled
```

### Docker image build notes

An image is available in the repo with public access, so it should not be necessary for the reader to run the steps below.

If you use a different docker registry, please change the URL for the image in the yaml files in the k8s directory.

## TL/DR - Docker image

```bash
git clone https://gitlab.com/john-auld/minikube-load-balanced.git
cd minikube-load-balanced
sudo su
docker login registry.gitlab.com
docker build -t registry.gitlab.com/john-auld/minikube-load-balanced:2.0 .
docker push registry.gitlab.com/john-auld/minikube-load-balanced:2.0
exit
```

Full documentation is given in docs/docker.md.

## Minikube instructions

## TL/DR - Deploy the manifests

```bash
minikube start
# Apply the manifests in the k8s directory
kubectl apply -f k8s
  deployment.apps/hello-world created
  ingress.extensions/hello-world-ingress created
  service/hello-world created
```

### Test access to the application

The following tests use features of curl which avoid the need to use host file entries in order to resolve the hostname used in this demo.

The container's hostname is included in the html content, to show which host responded to the request.

```bash
curl --resolve hello-world.example.com:80:$(minikube ip) http://hello-world.example.com/ && echo
  <p>Hello World! from hello-world-6bc6fbf5fc-gbmkk</p>

curl --resolve hello-world.example.com:80:$(minikube ip) http://hello-world.example.com/healthz && echo
  ok

# If you have an older version of curl, not having --resolve support
curl $(minikube ip) -H 'Host: hello-world.example.com' && echo
  <p>Hello World! from hello-world-6bc6fbf5fc-hgw4z</p>

curl $(minikube ip)/healthz -H 'Host: hello-world.example.com' && echo
  ok
```

Full documentation is given in docs/minikube.md
